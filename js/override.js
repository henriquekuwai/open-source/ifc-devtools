chrome.storage.sync.get('favoriteStores', (items) => {
    if (!!!items.favoriteStores || items.favoriteStores.length <=0) {
        chrome.storage.sync.set({ "favoriteStores": [] }, (items) => {
            console.log('Favoritos iniciados!');
        });
    }
});


function renderQuery() {
    var valor = $('#query').val();

    var legacy = ['lp', 'rb'];
    var item = `<div class="column is-one-quarter">
                    <a href="{{url}}" class="link">
                        <div class="box">
                            {{#icon}}
                                <i class="fa {{icon}}"></i>
                            {{/icon}}
                            <div class="tooltip {{ambiente}} {{tipo}}">{{tipo}}</div>
                            {{#ambiente}}<div class="tooltip {{.}}">{{.}}</div>{{/ambiente}}
                            {{nome}}
                        </div>
                    </a>
                </div>`;
    var urls = [
        {
            nome: 'Catálogo DEV',
            tipo: 'catalogo',
            ambiente: 'dev',
            url: 'http://www-{{storeId}}-dev.infracommerce.com.br',
            legacyurl: 'http://www.{{storeId}}.dev.accurate.com.br'
        },
        {
            nome: 'Admin DEV',
            tipo: 'admin',
            ambiente: 'dev',
            url: 'http://ad-{{storeId}}-dev.infracommerce.com.br/admin',
            legacyurl: 'http://ad.{{storeId}}.dev.accurate.com.br/admin',
        },
        {
            nome: 'Catálogo HCP',
            tipo: 'catalogo',
            ambiente: 'hcp',
            url: 'http://www-{{storeId}}-hcp.infracommerce.com.br'
        },
        {
            nome: 'Admin HCP',
            tipo: 'admin',
            ambiente: 'hcp',
            url: 'https://ad-hcp.a8e.net.br/admin/faces/login.jspx?st={{storeIdu}}'
        },
        {
            nome: 'Admin PRD',
            tipo: 'admin',
            ambiente: 'prd',
            url: 'https://ad.a8e.net.br/loja/{{storeId}}'
        },
        {
            nome: 'Hudson',
            tipo: 'tool',
            ambiente: '',
            url: 'http://lab.accurate.com.br/hudson/job/Templates-{{storeIdu}}'
        },
        {
            nome: 'SVN',
            tipo: 'acc',
            ambiente: '',
            url: 'https://lab.accurate.com.br/svn/acc/acec/templates/{{storeId}}'
        },
        {
            nome: 'Repositório GIT',
            tipo: 'acc',
            ambiente: '',
            url: 'https://gitlab.infracommerce.com.br/store-{{storeId}}/{{storeId}}-front',
        },
        {
            nome: 'Sitestudio',
            tipo: 'admin',
            ambiente: 'dev',
            url: 'http://ad-{{storeId}}-dev.infracommerce.com.br/icmanager/sitestudio/pagebuilder.jsp',
            legacyurl: 'http://ad.{{storeId}}.dev.accurate.com.br/icmanager/sitestudio/pagebuilder.jsp',
        },
        {
            nome: 'Genpar Table',
            tipo: 'admin',
            ambiente: 'dev',
            url: 'http://ad-{{storeId}}-dev.infracommerce.com.br/icmanager/cockpit/genpar.jsp',
            legacyurl: 'http://ad.{{storeId}}.dev.accurate.com.br/icmanager/cockpit/genpar.jsp',
        }
    ];
    var gerado = '';

    if (valor.length > 1) {
        for (var i = 0; i < urls.length; i++) {
            var islegacy = legacy.filter((el) => el == valor);
            var urlParsed = Mustache.render((islegacy.length > 0) ? urls[i].legacyurl : urls[i].url, {
                                            storeId: valor,
                                            storeIdu: valor.toUpperCase()
                                        });

            gerado += Mustache.render(item, {
                                        nome: urls[i].nome,
                                        url: urlParsed,
                                        tipo: urls[i].tipo,
                                        ambiente: urls[i].ambiente,
                                        icon: urls[i].icon
                                        });
        }
		$('.resultados').html(gerado);
    }

    chrome.storage.sync.set({ "lastStoreId": valor });
}

class FavoriteStores {
    constructor(options) {
        let default_options = {
            templates: {
				wrapper: `<span>Favoritos:</span><ul class="wrapper-favs">
                    {{#stores}}
                    <li id="{{.}}"><a href="#" class="favorite-item" data-storeid="{{.}}"><img src="http://i-{{.}}-dev.a8e.net.br/favicon/favicon.ico" />  {{.}} <span class="remove" data-storeid="{{.}}"><i class="fa fa-times"></i></span></a></li>
                    {{/stores}}
                </ul>`,
            }
        };

        this.options = _.merge({}, default_options, options);
        this.stores = [];
        chrome.storage.sync.get('favoriteStores', (items) => {
            this.stores = items.favoriteStores;
            console.log(this.stores);
        });
    }
    exists(storeId) {
        if (_.indexOf(this.stores, storeId) > -1) {
            return true;
        } else {
            return false;
        }
    }
    add(storeId) {
        if (_.indexOf(this.stores, storeId) > -1) {
            return false;
        }
        this.stores.push(storeId);
        this.saveStorage();
        return storeId;
    }
    remove(storeId, callback) {
        if (_.indexOf(this.stores, storeId) === -1) {
            return false;
        }
        this.stores.splice(_.indexOf(this.stores, storeId), 1);
        callback();
        this.saveStorage();
        return storeId;
    }
    saveStorage() {
        chrome.storage.sync.set({ "favoriteStores": this.stores }, (items) => {
            console.log('Favoritos salvos!');
        });
    }
    renderHtml() {
        if (!this.stores.length) {
            return false;
        }
        if (!this.options.templates.wrapper) {
            return false;
        }
        return Mustache.render(this.options.templates.wrapper, { stores: this.stores });
	}
	sortItens() {
		chrome.storage.sync.get('favoriteStores', (items) => {
			var $sortable = $(".wrapper-favs");
			var positions = items.favoriteStores;

			if (!!positions) {
				$.each(positions, function (i, position) {
					if (!!position) {
						var $target = $sortable.find('#' + position);
						$target.appendTo($sortable); // or prependTo for reverse
					}
				});
			}

			$sortable.sortable({
				update: () => {
					chrome.storage.sync.set({ "favoriteStores": $sortable.sortable("toArray") }, (items) => {
						console.log('Favoritos salvos!');
					});

					// favStores.saveNewOrder
				}
			});
		});
	}

}
window.favStores = new FavoriteStores();

$(document).ready(function () {

    let $query = $('#query');
    let $favorites = $('#favorites');
    let $btnAddFavorite = $('.add-to-favorite');

    /* get storage */
    chrome.storage.sync.get((items) => {

        if (!!items.lastStoreId) {
            $query.val(items.lastStoreId);
            renderQuery();

            if (favStores.exists($query.val())) {
                $btnAddFavorite.addClass('added');
            } else {
                $btnAddFavorite.removeClass('added');
            }
        }

        if (!!items.favoriteStores) {
            $favorites.html(favStores.renderHtml());
        }

	});

	$favorites.html(favStores.renderHtml());
	favStores.sortItens();



    /* parametros p/ gerar links */
    $query.on('keyup', function () {
        var $this = $(this);
        var valor = $this.val();
        if (favStores.exists(valor)) {
            $btnAddFavorite.addClass('added');
        } else {
            $btnAddFavorite.removeClass('added');
        }
        renderQuery();
    });

    $btnAddFavorite.on('click', function () {
        if (!$(this).hasClass('added') && !!$query.val()) {
            let valor = $query.val();
            $(this).addClass('added');
            favStores.add(valor);
			$favorites.html(favStores.renderHtml());
			favStores.sortItens()
        }
    });
    $favorites.on('click', '.wrapper-favs .remove', function () {
        let $this = $(this);
        let storeId = $this.attr('data-storeid');
        favStores.remove(storeId, () => {
			$favorites.html(favStores.renderHtml());
			favStores.sortItens();

            if (favStores.exists(storeId)) {
                $btnAddFavorite.addClass('added');
            } else {
                $btnAddFavorite.removeClass('added');
            }
        });
    });
    $favorites.on('click', '.wrapper-favs .favorite-item', function () {
        let $this = $(this);
        let storeId = $this.attr('data-storeid');
        $query.val(storeId);

        if (favStores.exists(storeId)) {
            $btnAddFavorite.addClass('added');
        } else {
            $btnAddFavorite.removeClass('added');
        }
        renderQuery();
    });

    var urlsfixas = [
        {
            nome: 'JIRA',
            tipo: 'acc',
            url: 'https://lab.accurate.com.br/request/secure/Dashboard.jspa'
        },
        {
            nome: 'Twiki',
            tipo: 'tool',
            url: 'https://lab.accurate.com.br/twiki'
        },
        {
            nome: 'Gitlab',
            tipo: 'acc',
            url: 'https://gitlab.infracommerce.com.br'
        },
        {
            nome: 'Kanboard',
            tipo: 'acc',
            url: 'http://kanboard.infracommerce.com.br:81'
        },
        {
            nome: 'RocketChat',
            tipo: 'tool',
            url: 'https://rocketchat.infracommerce.com.br'
        },
        {
            nome: 'Accweb',
            tipo: 'acc',
            url: 'http://lab.accurate.com.br/accweb/'
        },
        {
            nome: 'Solr DEV',
            tipo: 'solr',
            url: 'http://comp300:11510/acec-index46/#/'
        },
        {
            nome: 'Solr HCP',
            tipo: 'solr',
            url: 'http://comp364.acc.br:21610/acec-index/#/'
        },
        {
            nome: 'SVN - Componentes',
            tipo: 'acc',
            url: 'https://lab.accurate.com.br/svn/acc/acec/templates/components/trunk/'
        },
        {
            nome: 'Twiki - Front-End',
            tipo: 'acc',
            url: 'https://lab.accurate.com.br/twiki/bin/view/Main/InfrashopFrontEnd'
        },
        {
            nome: 'Base de conhecimento',
            tipo: 'acc',
            url: 'https://gitlab.infracommerce.com.br/infrashop/docs/issues'
        },
        {
            nome: 'Reset Password',
            tipo: 'tool',
            url: 'https://resetpassword.infracommerce.com.br/'
        }
    ];
    var itemfixo = `<div class="column is-one-quarter">
                        <a href="{{url}}" class="link">
                            <div class="box">
                                <div class="tooltip {{tipo}}">{{tipo}}</div>
                                {{nome}}
                            </div>
                        </a>
                    </div>`;
    var geradofixo = '';
    for (var i = 0; i < urlsfixas.length; i++) {
        geradofixo += Mustache.render(itemfixo, {
                                        nome: urlsfixas[i].nome,
                                        url: urlsfixas[i].url,
                                        tipo: urlsfixas[i].tipo
                                    });
    }
	$('.resultados-fixos').html(geradofixo);



});
