/*
** bloco de funções que independem do contexto
** ex: se tento chamar a URL do SOLR via AJAX no script injetado no DOM,
** recebo erro de cross-domain.
** se chamo daqui do inject.js, funciona corretamente.
*/
function injectScript(file, callback) {
    callback = typeof callback !== 'undefined' ? callback : '';

    var s = document.createElement("script");
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', file);
    document.head.appendChild(s);

    if (typeof callback === 'function') {
        s.onload = callback();
    }
}
function injectLink(file, callback) {
    callback = typeof callback !== 'undefined' ? callback : '';

    var s = document.createElement("link");
    s.setAttribute('rel', 'stylesheet');
    s.setAttribute('href', file);
    document.head.appendChild(s);

    if (typeof callback === 'function') {
        s.onload = callback();
    }
}

var solrInstance = '';
var solr = {
    dev: 'http://comp300:11510/acec-index46/',
    hcp: 'http://comp364.acc.br:21610/acec-index/'
}
var solrStatusDiv = '.solr-status';
var solrBtn = '.run-solr-btn';
var cacheBtn = '.run-cache-btn';

function runSolr (ambient, indice, withClean, retorno) {
    withClean = typeof withClean !== 'undefined' ? withClean : false;
    retorno = typeof retorno !== 'undefined' ? retorno : false;

	var beforeText = $(solrBtn).html();
	var isAjax = $(solrBtn).hasClass('goToIndex') ? false : true;

    if (!!isAjax) {
        $.ajax({
            url: solr[ambient] + indice + 'Index/dataimport',
            type: 'POST',
            dataType: 'json',
            data: 'command=full-import&clean=' + withClean + '&wt=json',
            success: function (data) {
                if (!!retorno) {
                    retorno(data);
                } else {
                    var messages = (data.statusMessages || {});

                    if (data.status == 'idle') {
                        $(solrStatusDiv).html('Índice executado!');
                    }

                    $(solrBtn).addClass('disabled').html('Processando...');
                    solrInstance = setInterval(function () {
                        var data = checkSolr(ambient, indice, function (data) {
							afterStatusActions(data, beforeText, solrInstance);
                        });
                    }, 1000);
                }
            },
            error: function () {
                alert('O índice parece estar inacessível.');
            }
        });
    } else {
        window.open(solr[ambient] + '#/' + indice + 'Index/dataimport');
    }
}
function checkSolr(ambient, indice, callback) {
    $.ajax({
        url: solr[ambient] + indice + 'Index/dataimport',
        type: 'GET',
        dataType: 'json',
        data: 'command=status&wt=json',
        success: function (data) {
            var messages = (data.statusMessages || {});
            var message = '';

            if (data.status == 'busy') {
                data = {
                    status: 'busy',
                    elapsedtime: messages['Time Elapsed']
                };
                message = 'Índice rodando há ' + data.elapsedtime + '...';
                data.message = message;

                callback(data);
            } else {
                data = {
                    status: 'idle',
                    fetched: messages['Total Rows Fetched'],
                    processed: messages['Total Documents Processed'],
                    elapsedtime: messages['Time taken']
                };
                message = 'Requisitado: ' + data.fetched + ' | Processado: ' + data.processed + ' | Tempo: ' + data.elapsedtime;
                data.message = message;

                callback(data);
            }
        },
        error: function () {
            callback({
				status: 'error',
				message: 'Falha ao realizar requisição.'
			});
        }
    });
}
function getUrl(url, callback) {
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            console.log('Limpeza do cache executada com sucesso.');
            callback(data);
        },
        error: function () {
            callback({
				status: 'error',
				message: 'Falha ao realizar requisição.'
			});
        }
    });
}
function afterStatusActions(data, beforeText, solrInstance) {
	$(solrStatusDiv).html(data.message);

	if (data.status == 'error') {
		$(solrStatusDiv).html(data.message);
		$(solrBtn).removeClass('disabled').addClass('goToIndex').html('Ir para índice');
		clearInterval(solrInstance);
	} else if (data.status == 'idle') {
		$(solrBtn).removeClass('disabled').html('Rodar índice');
		clearInterval(solrInstance);
	} else {
		$(solrBtn).addClass('disabled').html('Processando...');
	}
}

/*
** listener de integração com MESSAGES do JS injetado
** com isto, você pode emitir 'mensagens' do script injetado e
** executar alguma ação dependendo dos parâmetros.
** no caso, aqui, executa a função runSolr
*/
var port = chrome.runtime.connect();

window.addEventListener("message", function(event) {
  if (event.source != window)
    return;

  if (event.data.type) {
        if (event.data.type == "RUN_SOLR") {
            runSolr(event.data.ambient, event.data.indice, event.data.clean);
        }
        if (event.data.type == "STATUS_SOLR") {
          var beforeText = $(solrBtn).html();
          solrInstance = setInterval(function () {
				var data = checkSolr(event.data.ambient, event.data.indice, function (data) {
					afterStatusActions(data, beforeText, solrInstance);
				});
            }, 1000);
        }
        if (event.data.type == "RUN_CACHE") {
            var cacheBeforeText = $(cacheBtn).html();
			$(cacheBtn).addClass('disabled').html('Processando...');

            getUrl(event.data.url, function (data) {
                $(cacheBtn).removeClass('disabled').html(cacheBeforeText);
            });
        }
    }
}, false);

/* ------------ Injeção do script com acesso à var WINDOW */
injectLink('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

injectScript( chrome.extension.getURL('js/inject-with-window.js'), () => {
    setTimeout(() => {
        var urls = {
            jquery: chrome.runtime.getURL('js/jquery/jquery.min.js'),
            vendors: chrome.runtime.getURL('js/vendors.js'),
        };

        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent('sentScriptURLs', true, true, urls);
        document.dispatchEvent(evt);
    }, 200);
});